## Yarn Workspace
Yarn Workspace uses hoisting, so when you encounter a problem with the node module in app or web directory we can 
simply add the following

```
"workspace": {
  "nohoist": [
    "react-native",
    "react-native/**"
  ]
}
```

Add modules to the "nohoist" array as shown above.


## Mobx with React Hooks
First install

```
yarn add mobx-react-lite@1.0.1 mobx@5.9.0
```
Create Stores inside **src** directory inside the dev directory


**Mobx** : State Management Library

### @observer
Apply when all components that render observable data.

@observer also prevents re-renderings when the props of the component have only shallowly changed

observer also prevents re-renderings when the props of the component have only shallowly changed

## Setting up MonoRepo (dev/ repository)
### Directories in MonoRepo
    ./packages/dev/dist  : Output location from TypeScript compiler 
    ./packages/dev/src  : Input source for TypeScript compiler 

### package.json and tsconfig.json setup
In **./packages/dev/package.json**,

    “name”:”@hamin/dev” : @hamin is the name of MonoRepo and dev/ is the dependency name
    “main”:”dist/index.js” : dist/index.js is run as main is app/ and web/ when @hamin/dev is referenced 
    “build” : “rimraf dist && tsc” : Delete dist directory and rebuild TypeScript File.
    “devDependency”:{ ”rimraf”:”1.0.0" } : Install rimraf(any version)

In **./packages/dev/tsconfig.json**,

    "include":["src"] : Specify input source for TypeScript compiler
    "outdir":"dist" : Specify output directory location for TypeScript compiler
    "modules":"commonjs"
    "declaration":"true": Generate .d.ts file in TypeScript output
    “jsx”:”react"
    no “isolated modules” : we want the d.ts declarations generated
    no “allowjs” : TypeScript project
    no  “noemit” : We want to be able to emit the code

## App.tsx for app/ and web/
```js
import { App } from '@hamin/dev';

export default App;
```

## React Navigation & React Router
It works very well with **React Native** but not so well with **React Native Web**, especially when programming using 
**TypeScript**.

Instead I chose to use **React Router** package.

It has a similar API as React Navigation, and works better with the web.

Don't get me wrong, React Navigation is much better for React Native Projects.

## flags
Watch any changes and recompile it 

    yarn watch
    
This command flag is essential for the efficiency of this project as it reduces the time to manually
recomplie everytime we make changes in the **dev** directory


# Tools used to Analyze Sample UI 

[**awwapp**](https://awwapp.com/#) : Drawing board for UI component planning

[**Adobe Color**](https://color.adobe.com/create) : To customize color

# React Native Web Pitfalls

1) Hard to inspect on web since css is broken down to many classes.

2) Not so compatible with React Navigation
    
# Using [**mobx-persist**](https://github.com/pinqy520/mobx-persist)
Use MobX AsyncStorage instead of LocalStorage(which is synchronous).

# React Hooks Bug fix


## Minor notes
CSS Notes

    exitText: {
        fontSize: 30,
        color: '#F2F1E7'
    },
    
    Use color instead of fontColor
    
@computed filed

     ```
     This is used when we use existing fields to create a new field
     ```
     @computed displayTime() {
            const minutes = Math.floor(this.secondsRested / 60);
            const seconds = this.secondsRested;
    
            return `${minutes}:${seconds}`;
      }
      
      By using the `get` keyword we can acces this as a normal field
      
      @computed get displayTime() {
                  const minutes = Math.floor(this.secondsRested / 60);
                  const seconds = this.secondsRested;
          
                  return `${minutes}:${seconds}`;
      }
      
      Use case
      
      <WorkoutTimer
                      currentTime={rootStore.workoutTimerStore.displayTime}
                      onExitPress={() => {}}/>


