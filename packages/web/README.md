## Workout Planner

A simple workout planner to keep track of workout progress.

## Technology used

**[React Native Web](https://github.com/necolas/react-native-web/)**

State Management: **[Mobx](https://mobx.js.org/getting-started.html/)**

Programming Language: **[TypeScript](https://www.typescriptlang.org/)**

**[React Navigation](https://reactnavigation.org/)**

**[React Hooks](https://reactjs.org/docs/hooks-intro.html/)**

