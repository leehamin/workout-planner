import React from "react";
import { StyleSheet, View } from "react-native";
import { Routers } from "./Routers";

export const App = () => {
    return (
        <View style={styles.container}>
            <View style={styles.wrapper}>
                <Routers />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        height: "100%"
    },
    wrapper: {
        flex: 1,
        backgroundColor: "#283B46",
        width: "100%",
        maxWidth: 425
    }
});