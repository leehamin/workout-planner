import { observer } from "mobx-react-lite";
import * as React from "react";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";

interface Props {
    onExitPress: () => void;
    currentTime: string;
    progressPercent: string;
}

export const WorkoutTimer: React.FC<Props> = observer(
    ({onExitPress, currentTime, progressPercent}) => {
        return (
            <View style={styles.container}>
                <View style={[styles.progressLine, {width: progressPercent}]}/>
                <View style={styles.row}>
                    <Text style={styles.timerText}>
                        {currentTime}
                    </Text>
                    <TouchableOpacity onPress={onExitPress}>
                        <Text style={styles.exitText}>
                            X
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>

        );
    }
);

const styles = StyleSheet.create({
    container: {
        position: "absolute",
        bottom: 0,
        left: 0,
        height: 50,
        width: "100%",
        backgroundColor: '#E8A733'
    },
    exitText: {
        fontSize: 30,
        color: '#F2F1E7'
    },
    timerText: {
        fontSize: 20,
        color: '#fff'
    },
    row: {
        paddingHorizontal: 30,
        justifyContent: "space-between",
        flexDirection: "row",
        alignItems: "center",
        flex: 1
    },
    progressLine: {
        height: 3,
        backgroundColor: "#fff"
    }
});
