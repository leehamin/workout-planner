import { observer } from "mobx-react-lite";
import * as React from "react";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";

interface Props {
    exercise: string,
    repsAndWeight: string,
    duration?: number
    sets: string[],
    onButtonPress: (index: number) => void
}

export const WorkoutCard: React.FC<Props> = observer(
    ({exercise, repsAndWeight, duration, sets, onButtonPress}) => {
        return (
            <View style={styles.cardContainer}>
                <View style={styles.topRow}>
                    <Text style={styles.topRowText}>{exercise}</Text>
                    <Text style={styles.topRowText}>{repsAndWeight}</Text>
                </View>
                <View style={styles.bottomRow}>
                    {sets.map((set, index) => {
                        if (set == "x") {
                            return (
                                <View
                                    style={[styles.circle, styles.fadedBackground]}
                                    key={set + index}>
                                    <Text
                                        style={[styles.circleText, styles.whiteText]}
                                        key={set + index}>X</Text>
                                </View>
                            )
                        }
                        if (set == "") {
                            return (
                                <TouchableOpacity
                                    onPress={() => onButtonPress(index)}
                                    style={styles.circle}
                                    key={set + index}/>
                            )
                        }
                        else {
                            return (
                                <TouchableOpacity
                                    onPress={() => onButtonPress(index)}
                                    style={styles.circle}
                                    key={set + index}>
                                    <Text
                                        style={[styles.circleText, styles.whiteText]}>
                                        {set}
                                    </Text>
                                </TouchableOpacity>
                            )
                        }
                    })}
                </View>
                <View style={styles.durationText}>
                    <Text style={styles.whiteText}>
                        Duration:
                    </Text>
                    <Text> </Text>
                    <Text style={styles.whiteText}>
                        {duration} Minutes
                    </Text>
                </View>

            </View>

        );
    }
);

const styles = StyleSheet.create({
    cardContainer: {
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10
    },
    topRow: {
        flexDirection: "row",
        justifyContent: "space-between"
    },
    topRowText: {
        fontSize: 17,
        color: "#fff"
    },
    bottomRow: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 15
    },
    circle: {
        height: 50,
        width: 50,
        borderRadius: 25,
        backgroundColor: "#FF872F"
    },
    circleText: {
        fontSize: 17,
        color: "#fff",
        margin: "auto"
    },
    durationText: {
        marginTop: 10,
        flexDirection: "row",
        justifyContent: "flex-end"
    },
    whiteText: {
        color: "#fff"
    },
    fadedBackground: {
        backgroundColor: "#B2A1A1"
    }
});
