import React from "react";
import { CurrentWorkout } from "./screens/CurrentWorkout";
import { WorkoutHistory } from "./screens/WorkoutHistory";
import { Route, Router, Switch } from "./router/index";

export const Routers = () => {
    return (
        <Router>
            <Switch>
                <Route exact path="/" component={WorkoutHistory} />
                <Route exact path="/current-workout" component={CurrentWorkout} />
                <Route
                    exact
                    path="/workout/:year/:month/:day"
                    component={CurrentWorkout}
                />
            </Switch>
        </Router>
    );
};
