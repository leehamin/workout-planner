import * as React from "react";
import {Button, StyleSheet, Text, View, Image, FlatList, ImageBackground} from "react-native";
import {observer} from "mobx-react-lite";
import {RootStoreContext} from "../stores/RootStore";
import {RouteComponentProps} from "react-router";
import {WorkoutHistoryCard} from "../components/WorkoutHistoryCard";
import { CardComponent } from "../components/CardComponent";
import { CurrentExercise } from "../stores/WorkoutStore";



interface Props extends RouteComponentProps {}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scrollContainer: {
        padding: 10,
        marginBottom: 50
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
    },
    row: {
        flexDirection: "row"
    },
    centerText: {

    }
});



export const WorkoutHistory: React.FC<Props> = observer(({history}) => {
    const rootStore = React.useContext(RootStoreContext);
    const resizeMode = 'center';
    const rows: Array<
        Array<{
            date: string;
            exercises: CurrentExercise[];
        }>
        > = [];

    Object.entries(rootStore.workoutStore.history).forEach(
        ([date, exercises], i) => {
            if (i % 3 === 0) {
                rows.push([
                    {
                        date,
                        exercises
                    }
                ]);
            } else {
                rows[rows.length - 1].push({
                    date,
                    exercises
                });
            }
        }
    );

    return (
        <View
            style={{
                flex: 1,
                backgroundColor: '#eee',
            }}
        >
            <View
                style={{
                    flex: 1,
                    backgroundColor: 'transparent',
                    justifyContent: 'center',
                }}
            >
                <Button title={"Create Workout"} onPress={() => {
                    rootStore.workoutStore.currentExercises.push({
                            weight: 145,
                            reps: 5,
                            numSets: 3,
                            exercise: "Bench Press",
                            sets: ["","","","",""],
                            duration: 45
                        },{
                            weight: 210,
                            reps: 5,
                            numSets: 4,
                            exercise: "Squats",
                            sets: ["","","","","x"],
                            duration: 45
                        },{
                            weight: 200,
                            reps: 5,
                            numSets: 4,
                            exercise: "Dead Lift",
                            sets: ["","","","","x"],
                            duration: 45
                        },
                        {
                            weight: 130,
                            reps: 5,
                            numSets: 5,
                            exercise: "Rows",
                            sets: ["","","","",""],
                        });
                    history.push('/current-workout');
                }}/>
            </View>
        </View>


    );
});
