import * as React from "react";
import {Button, View, StyleSheet, ScrollView} from "react-native";
import {observer} from "mobx-react-lite";
import {WorkoutCard} from "../components/WorkoutCard";
import {RootStoreContext} from "../stores/RootStore";
import {WorkoutTimer} from "../components/WorkoutTimer";
import {RouteComponentProps} from "react-router";
import dayjs from "dayjs";

interface Props extends RouteComponentProps{}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    scrollContainer: {
        padding: 10,
        marginBottom: 5
    },
    marginTop: {
        marginTop: 8
    }
});


export const CurrentWorkout: React.FC<Props> = observer(
    ({
         history
    }) => {
    const rootStore = React.useContext(RootStoreContext);

    React.useEffect(() => {
        return () => {
            rootStore.workoutTimerStore.endTimer();
        };
    }, []);

    return (
        <View style={styles.container}>
            <ScrollView
                keyboardShouldPersistTaps="always"
                contentContainerStyle={styles.scrollContainer}
            >
                {rootStore.workoutStore.currentExercises.map(exercise => {
                    return(
                        <WorkoutCard
                            onButtonPress={currentIndex => {
                                rootStore.workoutTimerStore.startTimer();
                                const currentValue = exercise.sets[currentIndex];

                                let updatedValue: string;

                                if (currentValue === '') {
                                    updatedValue = `${exercise.reps}`
                                }
                                else if (currentValue === '0') {
                                    updatedValue = ''
                                } else {
                                    updatedValue = `${parseInt(currentValue) - 1}`
                                }

                                exercise.sets[currentIndex] = updatedValue;
                            }}
                            exercise={exercise.exercise}
                            repsAndWeight={`${exercise.numSets} x ${exercise.reps}  ${exercise.weight}`}
                            duration={rootStore.workoutTimerStore.previousRestTime}
                            sets={exercise.sets}/>
                    )
                })}
                <Button
                    title={'SAVE'}
                    onPress={() => {
                    rootStore.workoutStore.history[dayjs().format("YYYY-MM-DD")] =
                        rootStore.workoutStore.currentExercises;
                        rootStore.workoutStore.currentExercises = [];
                        history.push('/')
                    }}
                />

                <View style={styles.marginTop}>
                </View>

                <Button
                    title={'BACK'}
                    onPress={() => {
                        rootStore.workoutStore.currentExercises = [];
                        history.push('/')
                    }}
                    color="#FF872F"
                />

            </ScrollView>
            {rootStore.workoutTimerStore.isRunning ? <WorkoutTimer
                progressPercent={rootStore.workoutTimerStore.progressPercent}
                currentTime={rootStore.workoutTimerStore.displayTime}
                onExitPress={() => {rootStore.workoutTimerStore.endTimer()}}/>
                : null}

        </View>
    )
});

