import {RouterStore} from "./RouterStore";
import {WorkoutStore} from "./WorkoutStore";
import {createContext} from "react";
import {WorkoutTimerStore} from "./WorkoutTimerStore";
import {AsyncStorage} from "react-native";
import {create} from "mobx-persist";

const hydrate = create({
    storage: AsyncStorage,   // or AsyncStorage in react-native.
    // React Native Web implements AsyncStorage
    // default: localStorage
    jsonify: true  // if you use AsyncStorage, here should be true
    // default: true
});

export class RootStore {
    // Pass in "this" to access the individual stores
    workoutStore = new WorkoutStore(this);
    workoutTimerStore = new WorkoutTimerStore();

    constructor() {
        // Checking if its localStorage and persist the data overtime
        hydrate("workoutTimer", this.workoutTimerStore).then(() => {
            if (this.workoutTimerStore.isRunning) {
                this.workoutTimerStore.measure();
            }
        });
        hydrate("workout", this.workoutStore);
    }

}


export const RootStoreContext = createContext(new RootStore());