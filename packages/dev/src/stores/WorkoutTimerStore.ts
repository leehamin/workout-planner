import {observable, action, computed} from "mobx";
import dayjs = require("dayjs");
import {persist} from "mobx-persist";


export class WorkoutTimerStore {
    @persist('object') @observable startTime = dayjs();
    @persist @observable isRunning = false;
    @persist @observable secondsRested = 0;
    @persist @observable totalWorkOutTime  = 0;
    @persist @observable previousRestTime = 0;



    @action measure() {
        if (!this.isRunning) {
            return;
        }
        this.secondsRested = dayjs().diff(this.startTime, "second");

        setTimeout(() => this.measure(), 1000);
    }

    @action startTimer() {
        this.isRunning = true;
        this.startTime = dayjs();
        this.measure();
    }

    @action endTimer() {
        this.isRunning = false;
        this.secondsRested = 0;
    }

    @computed get displayTime() {
        const minutes = Math.floor(this.secondsRested / 60);
        const seconds = this.secondsRested % 60;
        if (seconds < 10) {
            return `${minutes}:0${seconds}`;
        } else {
            return `${minutes}:${seconds}`;
        }
    }

    @computed get progressPercent() {
        const seconds = this.secondsRested;
        const progressPercent = Math.min(100, (seconds / 90) * 100)
        return `${progressPercent}%`;
    }


}


