import {RootStore} from "./RootStore";
import {computed, observable} from "mobx";
import {persist} from "mobx-persist";

type WorkoutDay = "a" | "b";

/*
Data Structure => Use date as key
{
    '05-01-2019': [
        {
            exercise: "Bench Press";
            value: 150;
        },
        {
            exercise: "Squats";
            value: 200;
        }

    ]
}
 */
interface WorkoutHistory {
    [key: string]: CurrentExercise[];
}

export interface CurrentExercise {
    weight: number;
    reps: number;
    numSets: number;
    exercise: string;
    sets: string[];
    duration?: number
}


export class WorkoutStore {
    rootStore: RootStore;
    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
    }
    @persist @observable currentSquat: number = 45;
    @persist @observable currentBenchPress: number = 45;
    @persist @observable currentOverheadPress: number = 45;
    @persist @observable currentDeadlift: number = 65;
    @persist @observable currentBarbellRow: number = 65;

    @persist @observable lastWorkoutType: WorkoutDay = "a";

    @persist("list") @observable currentExercises: CurrentExercise[] = [];

    @computed get hasCurrentWorkout() {
        return !!this.currentExercises.length;
    }

    @persist("object") @observable history: WorkoutHistory = {};
}


