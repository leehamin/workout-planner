## Workout Planner

A simple workout planner to keep track of workout progress.

## How to Use
1) Create Your Workout

2) Press the circle and enter the number of reps you did for that set.

    In other words, one circle is one set.

3) Timer runs to keep track of your rest time.
    
4) Your workout will be automatically saved in localStorage so that you would be able to access it
in the future.

## Technology used

**[React Native Web](https://github.com/necolas/react-native-web/)**

State Management: **[Mobx](https://mobx.js.org/getting-started.html/)**

Programming Language: **[TypeScript](https://www.typescriptlang.org/)**

**[React Navigation](https://reactnavigation.org/)**

**[React Hooks](https://reactjs.org/docs/hooks-intro.html/)**

